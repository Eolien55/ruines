# Ruines

`Ruines` est un jeu vidéo qui explore les décombre d'un royaume disparu. Le jeu
en tant que tel (code) n'est pas encore très développé, mais l'univers est
assez avancé.

## License

Le code (c'est-à-dire tout ce qui est dans `src`) est sous GPLv2, et
les assets (sprites, musiques, texte, etc.) sont propriété des créateurs.

Copyright (c) 2019-2022 Eolien55, Timinou, Takki
