extends KinematicBody2D

# Vitesse, en pixel / seconde
export var speed = 300
var screen_size
var velocity = Vector2.ZERO

onready var sprite = $AnimatedSprite

# Called when the node enters the scene tree for the first time.
func _ready():
	sprite.play()
	screen_size = get_viewport_rect().size	


func handle_input():
	if Input.is_action_pressed("ui_right"):
		velocity.x = 1
	if Input.is_action_pressed("ui_left"):
		velocity.x = -1

func _physics_process(delta):
	handle_input()
	if velocity.x != 0:
		sprite.flip_h = velocity.x == -1
		sprite.play("walk")
	else:
		sprite.play("idle")
