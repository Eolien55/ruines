{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 15,
  "bbox_right": 84,
  "bbox_top": 20,
  "bbox_bottom": 79,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 100,
  "height": 100,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3c4bb593-7a17-4a4e-bbbb-a71d35ed2e68","path":"sprites/sPlayer_death/sPlayer_death.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3c4bb593-7a17-4a4e-bbbb-a71d35ed2e68","path":"sprites/sPlayer_death/sPlayer_death.yy",},"LayerId":{"name":"578b805f-2d07-4b5d-b4c3-0fd5199cf8bf","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayer_death","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","name":"3c4bb593-7a17-4a4e-bbbb-a71d35ed2e68","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"40897cc0-9ed6-451b-9807-bd407c373fa8","path":"sprites/sPlayer_death/sPlayer_death.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"40897cc0-9ed6-451b-9807-bd407c373fa8","path":"sprites/sPlayer_death/sPlayer_death.yy",},"LayerId":{"name":"578b805f-2d07-4b5d-b4c3-0fd5199cf8bf","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayer_death","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","name":"40897cc0-9ed6-451b-9807-bd407c373fa8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"239d87bb-cafd-4336-8210-d09e68acc862","path":"sprites/sPlayer_death/sPlayer_death.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"239d87bb-cafd-4336-8210-d09e68acc862","path":"sprites/sPlayer_death/sPlayer_death.yy",},"LayerId":{"name":"578b805f-2d07-4b5d-b4c3-0fd5199cf8bf","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayer_death","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","name":"239d87bb-cafd-4336-8210-d09e68acc862","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b4901c81-e1b5-4c99-94c3-0112cf7f9742","path":"sprites/sPlayer_death/sPlayer_death.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b4901c81-e1b5-4c99-94c3-0112cf7f9742","path":"sprites/sPlayer_death/sPlayer_death.yy",},"LayerId":{"name":"578b805f-2d07-4b5d-b4c3-0fd5199cf8bf","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayer_death","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","name":"b4901c81-e1b5-4c99-94c3-0112cf7f9742","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sPlayer_death","path":"sprites/sPlayer_death/sPlayer_death.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"8de0e607-233a-4b73-a777-b10ec1dfcb7c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3c4bb593-7a17-4a4e-bbbb-a71d35ed2e68","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"31307635-1288-4a9d-9ba6-3813881eeaff","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"40897cc0-9ed6-451b-9807-bd407c373fa8","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5adeb2f4-f1b6-4761-a9e6-0bdd8402789b","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"239d87bb-cafd-4336-8210-d09e68acc862","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5e09b4ba-98d6-49c7-8513-f7d7405d3016","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b4901c81-e1b5-4c99-94c3-0112cf7f9742","path":"sprites/sPlayer_death/sPlayer_death.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sPlayer_death","path":"sprites/sPlayer_death/sPlayer_death.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"578b805f-2d07-4b5d-b4c3-0fd5199cf8bf","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sPlayer_death",
  "tags": [],
  "resourceType": "GMSprite",
}