# Ruines

`Ruines` is a french video game that explores the _ruins_ of an old fallen
kingdom (see, we have many humour). The code of the game is in **very** early
phase, but the universe is quite advanced.

## Licensing

The code (ie, everything in the `src` directory) is licensed under
the GPLv2, while the art (ie, everything in the `assets` directory) is
owned by the creators.

Copyright (c) 2019-2022 Eolien55, Timinou, Takki
